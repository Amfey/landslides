from django.test import TestCase

from .models import Landslide

# Create your tests here.

class HomePageTest(TestCase):

    def test_homepage_should_using_correct_template_when_is_checking(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, "index.html")

    def test_homepage_should_have_header_of_landslide_list_when_request(self):
        response = self.client.get('/')
        self.assertIn('<h1>A list of landslides</h1>', response.content.decode())

    def test_Landslide_should_name_as_location_when_asking_about_name(self):
        name_landslide = Landslide(location = "Tarnow")
        self.assertEqual("Tarnow", name_landslide.location)

    def test_home_page_should_save_when_POST_request(self):
        Landslide.objects.create(location='Somewhere')
        self.assertEqual(Landslide.objects.count(), 1)

    def test_home_page_should_show_table_of_records_when_is_viewing(self):
        Landslide.objects.create(location='Tarnow')
        Landslide.objects.create(location='Krakow')

        response = self.client.get('/')

        self.assertIn('Tarnow', response.content.decode())
        self.assertIn('Krakow', response.content.decode())

    #def test_should_fail_when_running(self):
    #    self.fail("Test fail anytime!!")