from django.shortcuts import render, redirect
from django.http import HttpResponse
from polls.models import Landslide
# Create your views here.

def index(request):
    if request.method == 'POST':
        Landslide.objects.create(location=request.POST['location'])
        return redirect('index')

    locations = Landslide.objects.all()
    return render(request, 'index.html', {'locations': locations})